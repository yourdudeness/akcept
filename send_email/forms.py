from django import forms

from .models import SendMail, OfferRequest

class SendMailForm(forms.ModelForm):

    class Meta:
        model = SendMail
        fields = [
            'user_name','user_phone', 'user_message'
        ]

class BussinesOfferForm(forms.ModelForm):
    class Meta:
        model = OfferRequest
        fields = [
            'name', 'email'
        ]