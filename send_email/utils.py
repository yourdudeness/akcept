from django.core.mail import send_mail
from django.conf import settings


def send_email_request(user_name, user_mail, user_message):
    mail_title = 'Обратный звонок с сайта Accept'
    mail_body = 'Обратный звонок\n'
    mail_body += 'Имя: %s\n' % user_name
    mail_body += 'Телефон: %s\n' % user_phone
    mail_body += 'Сообщение: %s\n' % user_message
    send_mail(mail_title, mail_body, 'localhost@server.ru')

def send_offer_mail(name, email):
    mail_title = 'Запрос на коммерческое предложение с сайта Accept'
    mail_body = 'Запрос на коммерческое предложение\n'
    mail_body += 'Ф.И.О: %s\n' % name
    mail_body += 'Email: %s\n' % email
    send_mail(mail_title, mail_body, 'localhost@server.ru')