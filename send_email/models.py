from django.db import models

# Create your models here.
class SendMail(models.Model):
    user_name = models.CharField(verbose_name = 'Name', max_length=256)
    user_phone = models.CharField(verbose_name = 'Phone', max_length=18)
    user_message = models.CharField(verbose_name = 'Message', max_length=256)

class OfferRequest(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=256)
    email = models.CharField(verbose_name='Email', max_length=256)