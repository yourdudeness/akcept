from django.urls import path

from . import views

urlpatterns = [
    path('send_email/', views.SendMailView.as_view(), name='send_email'),
    path('offer_email/', views.BussinesOfferView.as_view(), name ='offer_email')
]