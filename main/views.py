from django.shortcuts import render
from django.views.generic import TemplateView


class ItemSite(TemplateView):
    template_name = 'index.html'

class CompanyPractices(TemplateView):
    template_name = 'perfomance.html'

class QualityAssurance(TemplateView):
    template_name = 'quality.html'

class TechnologyAndSafety(TemplateView):
    template_name = 'safety.html'

class Trust(TemplateView):
    template_name = 'trust.html'
