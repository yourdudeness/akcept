from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.ItemSite.as_view(), name = 'index'),
    path('practices', views.CompanyPractices.as_view(), name = 'practice'),
    path('quality-assurance', views.QualityAssurance.as_view(), name ='quality'),
    path('technology-and-safety', views.TechnologyAndSafety.as_view(), name = 'safety'),
    path('trust', views.Trust.as_view(), name = 'trust'),

]