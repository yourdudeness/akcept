$(window).on('load', function () {

  $(window).scroll(function () {

    if ($(this).scrollTop() != 0) {

      $('#toTop').fadeIn();

    } else {

      $('#toTop').fadeOut();

    }

  });

  $('#toTop').click(function () {

    $('body,html').animate({
      scrollTop: 0
    }, 500);

  });

  $('[data-src]').fancybox({
    toolbar: false,
    smallBtn: true,
    touch: false,
    autoFocus: false,
    // afterLoad: function () {
    // bodyScrollLock.disableBodyScroll(document.querySelector('.fancybox-slide'));
    // clearForm();

    //modalwindow
    afterLoad: function () {
      var fancyboxSlide = document.querySelectorAll('.fancybox-slide');
      fancyboxSlide.forEach(function (element) {
        bodyScrollLock.disableBodyScroll(element);
      });
    },
    beforeClose: function () {
      if ($('.fancybox-slide').length == 1) {
        bodyScrollLock.clearAllBodyScrollLocks();
        $(".modal-callback__item-form").trigger('reset');
      }
    },
  });


  //SLICK    
  $('.slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 600,
    fade: true,
    cssEase: 'linear',
    draggable: false
  });


  $('.comments__slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    autoplay: true
  });


  //PROGRESS BAR
  if ($('#container').length) {
    function progressBar() {
      var bar = new ProgressBar.Circle(container, {

        color: '#5e4ac5',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 5,
        trailWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: false
        },
        from: {
          color: '#5e4ac5',
          width: 1
        },
        to: {
          color: '#5e4ac5',
          width: 3
        },
        // Set default step function for all animate calls
        step: function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 100);
          if (value === 0) {
            circle.setText('');
          } else {
            circle.setText(value + '%');
          }

        }

      });

      bar.text.style.fontFamily = '"Hind",sans-serif;';
      bar.text.style.fontSize = '5rem';
      bar.text.style.fontWeight = 700;
      if ($(window).width() < 1025) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 500;
      }

      if ($(window).width() < 701) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '5rem';
        bar.text.style.fontWeight = 700;
      }
      bar.animate(0.97);
    }
    $(function () {
      var jqBar = $('.efficiency');
      var jqBarStatus = true;
      $(window).scroll(function () {
        var scrollEvent = ($(window).scrollTop() > (jqBar.position().top - $(window).height()));
        if (scrollEvent && jqBarStatus) {
          jqBarStatus = false;
          progressBar();
        }
      });
    });
  }

  if ($('#container2').length) {
    function bar2() {
      var bar = new ProgressBar.Circle(container2, {
        color: '#5e4ac5',
        strokeWidth: 5,
        trailWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: true
        },
        from: {
          color: '#5e4ac5',
          width: 2
        },
        to: {
          color: '#5e4ac5',
          width: 3
        },
        // Set default step function for all animate calls
        step: function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 100);
          if (value === 0) {
            circle.setText('');
          } else {
            circle.setText(value + '%');
          }

        }
      });
      bar.text.style.fontFamily = '"Hind",sans-serif;';
      bar.text.style.fontSize = '5rem';
      bar.text.style.fontWeight = 700;
      if ($(window).width() < 1025) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 500;
      }
      if ($(window).width() < 701) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '5rem';
        bar.text.style.fontWeight = 700;
      }
      bar.animate(0.97);


    }

  }
  $(function () {
    var jqBar = $('.efficiency');
    var jqBarStatus = true;
    $(window).scroll(function () {
      var scrollEvent = ($(window).scrollTop() > (jqBar.position().top - $(window).height()));
      if (scrollEvent && jqBarStatus) {
        jqBarStatus = false;
        bar2();
      }
    });
  });


  if ($('#container3').length) {
    function bar3() {
      var bar = new ProgressBar.Circle(container3, {
        color: '#5e4ac5',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 5,
        trailWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: false
        },
        from: {
          color: '#5e4ac5',
          width: 2
        },
        to: {
          color: '#5e4ac5',
          width: 3
        },
        // Set default step function for all animate calls
        step: function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 10);
          if (value === 0) {
            circle.setText('');
          } else {
            circle.setText(value + ' из 10');
          }

        }

      });
      bar.text.style.fontFamily = '"Hind",sans-serif;';
      bar.text.style.fontSize = '3.5rem';
      bar.text.style.fontWeight = 700;
      if ($(window).width() < 1025) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 500;
      }
      if ($(window).width() < 801) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '3rem';
        bar.text.style.fontWeight = 700;
      }

      if ($(window).width() < 701) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 700;
      }

      if ($(window).width() < 481) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '2rem';
        bar.text.style.fontWeight = 700;
      }

      bar.animate(0.93); // Number from 0.0 to 1.0
    }
    $(function () {
      var jqBar = $('.efficiency');
      var jqBarStatus = true;
      $(window).scroll(function () {
        var scrollEvent = ($(window).scrollTop() > (jqBar.position().top - $(window).height()));
        if (scrollEvent && jqBarStatus) {
          jqBarStatus = false;
          bar3();
        }
      });
    });

  }

  if ($('#container4').length) {
    function bar4() {
      var bar = new ProgressBar.Circle(container4, {
        color: '#5e4ac5',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 5,
        trailWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: false
        },
        from: {
          color: '#5e4ac5',
          width: 2
        },
        to: {
          color: '#5e4ac5',
          width: 3
        },
        // Set default step function for all animate calls
        step: function (state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);

          var value = Math.round(circle.value() * 7);
          if (value === 0) {
            circle.setText('');
          } else {
            circle.setText(value + ' из 10');
          }

        }
      });
      bar.text.style.fontFamily = '"Hind",sans-serif;';
      bar.text.style.fontSize = '3.5rem';
      bar.text.style.fontWeight = 700;
      if ($(window).width() < 1025) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 500;
      }

      if ($(window).width() < 801) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '3rem';
        bar.text.style.fontWeight = 700;
      }

      if ($(window).width() < 701) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '4rem';
        bar.text.style.fontWeight = 700;
      }

      if ($(window).width() < 481) {
        bar.text.style.fontFamily = '"Hind",sans-serif;';
        bar.text.style.fontSize = '2rem';
        bar.text.style.fontWeight = 700;
      }

      bar.animate(0.97);



    }
  }

  $(function () {
    var jqBar = $('.efficiency');
    var jqBarStatus = true;
    $(window).scroll(function () {
      var scrollEvent = ($(window).scrollTop() > (jqBar.position().top - $(window).height()));
      if (scrollEvent && jqBarStatus) {
        jqBarStatus = false;
        bar4();
      }
    });
  });


  $('.modal__close').on('click', function () {
    var modal = $(this).parents('.modal');
    $.fancybox.close({
      src: modal,
      type: 'inline',
    });
    $(".modal-callback__item-form").trigger('reset');
    $(".modal-callback__item-form-btn").attr('disabled', true);
  });


  $('.form__checkbox:checkbox').change(function () {
    var check = $(this).prop('checked');
    if (check) {
      $(this).parents('form').children('button[type="submit"]').removeAttr('disabled');
    } else {
      $(this).parents('form').children('button[type="submit"]').attr('disabled', true);
    }

  });


  $(".container__nav-item-link").click(function () {
    $(this).toggleClass('link--active');
    return false;
  });


  var mobileMenu = document.querySelector('.menu-mobile');
  $('.container__burger, .menu-mobile__close, .container__nav-item-link-submenu-linked, .container__nav-item-link-submenu-linked, .container__nav-item-link-submenu-linked, .container__nav-item-call').click(function () {

    if (!$(".menu-mobile").is(":visible")) {
      bodyScrollLock.disableBodyScroll(mobileMenu);
    } else {
      bodyScrollLock.clearAllBodyScrollLocks();
    }
    $(".menu-mobile").toggle('fast', function () {});
  });

  $(document).scroll(function () {
    var $nav = $(".navbar-fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });

  $('.modal-callback__item-form, .wrap__item-desc-form').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).attr('action');

    var data = {
      csrfmiddlewaretoken: $(this).find('input[name="csrfmiddlewaretoken"]').val()
  };


    $(this).find('.modal-callback__item-form-point, .wrap__item-desc-form-field').each(function () {
      data[$(this).attr('name')] = $(this).val();
    });

    


    var url = $(this).attr('action');
    $.ajax(url, {
      method: 'post',
      data: data,
      success: function (response) {
        
        //Здесь модальник

        $.fancybox.close({
          src: '.modal',
          type: 'inline',
        });
    
        // console.log(data);
    
        $.fancybox.open({
          src: '#modal-feedback',
          type: 'inline',
          opts: {
            afterLoad: function () {
              var fancyboxSlide = document.querySelectorAll('.fancybox-slide');
              fancyboxSlide.forEach(function (element) {
                bodyScrollLock.disableBodyScroll(element);
              });
            },
            beforeClose: function () {
              if ($('.fancybox-slide').length == 1) {
                bodyScrollLock.clearAllBodyScrollLocks();
                $(".modal-callback__item-form, .wrap__item-desc-form").trigger('reset');
              }
            },
          }
        }, {
          touch: false,
        });

      }
    });



  });

  $(".slide__item").fadeIn(2000);



  $('.container__nav-menu-item-linked, .item__topcontent-columns-headline, .container__nav-menu-submenu-item-link, .item__topcontent-columns-link, .slide__item-wrap-btn').click(function () {
    var anch = this.hash.slice(0);

    if (!anch || !anch[0] === "#") return;
    // e.preventDefault();
    // window.location.hash = '';
    var offset = $(anch).offset();
    $("html, body").animate({
      scrollTop: $(anch).offset().top
    }, 1000);
    // if(history.pushState) { history.pushState({}, null, window.location.pathname); }
  });

  // $('.container__nav-menu-submenu-item-link').click(function (e) {
  //   var subhide = $(".container__nav-menu-submenu");

  //   if (!subhide.is(e.target)) {
  //     subhide.hide();
  //   }if (subhide.is(e.target)) {
  //     s
  //   }else{
  //     return true;
  //   }
  // });

  var phoneMask = ['+',7,'(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  // Assuming you have an input element in your HTML with the class .myInput
  var myInput = document.getElementById('number')

  var maskedInputController = vanillaTextMask.maskInput({
    inputElement: myInput,
    mask: phoneMask,
  })

});
