if($('#map').length){
    var map = document.querySelector('#map');

var onIntersection = function (entries) {
    for (const entry of entries) {
        if (entry.intersectionRatio > 0) {
            ymaps.ready(mapsInit);
            observer.unobserve(entry.target);
        }
    }
};

var observer = new IntersectionObserver(onIntersection);
observer.observe(map);

function mapsInit() {
    myMap = new ymaps.Map('map', {
        center: ($(window).width() <= '768') ? [56.309634, 44.070351] : [56.309634, 44.070351],
        zoom: 17,
        controls: ['zoomControl']
    });
    var Nizhniy = new ymaps.Placemark([56.309634, 44.070351], {
        balloonContent: ''
    }, {
        hasBalloon: false,
        iconLayout: 'default#image',
        // iconImageHref: '/img/marker.png',
        iconImageSize: [43, 47],
        iconImageOffset: [-32, -77]
    });
    myMap.geoObjects.add(Nizhniy);
    myMap.behaviors.disable('scrollZoom');
};

}
