//Подклюбчение модуля галпа
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    del = require('del'),
    concat = require('gulp-concat'),
    terser = require('gulp-terser'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    imageminPngquant = require('imagemin-pngquant'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    webp = require('gulp-webp'),
    log = require('fancy-log');


//Порядок подключения css файлов

//Порядок подключения js файлов


function shakaling() {
    return gulp
        .src('./src/img/*.*')
        .pipe(
            imagemin([
              imagemin.jpegtran({ progressive: true }),
              imagemin.optipng({ optimizationLevel: 5 }),
              ]),
          )
        .pipe(gulp.dest('./build/img/'));
}

function img(){
    return gulp
    .src('./src/img/*.*')
    .pipe(gulp.dest('./build/img/'));
}

function watch() {
    gulp.watch('./src/css/**/*.scss', styles);
    gulp.watch('./src/js/*.js', scripts);
    gulp.watch('./src/*.html', minhtml);
    gulp.watch('./src/img/*.*', shakaling);
}

//Таск на стили css
function styles() {
    //Шаблон для поиска файлов CSS
    //Все файлы по шаблону '.src/css/**/ *.css'

    return gulp.src('./src/css/main.scss')
        .pipe(sass().on('error', sass.logError))
        // .pipe(autoprefixer('last 2 versions'))
        .pipe(autoprefixer({
            cascade: false
        }))
        //Выходная папка для стилей
        .pipe(cleanCSS({
            level: 2
        }))

        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());

}

function minhtml() {
    return gulp.src('./src/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./build'))
        .pipe(gulp.dest('./templates'));
}


//Таск на скрипты js
function scripts() {
    return gulp.src(['./src/js/jquery.js', './src/js/vanillaTextMask.js', './src/js/slick.js', './src/js/progressbar.js', './src/js/jquery.fancybox.min.js', './src/js/bodyScrollLock.min.js', './src/js/ymap.js', './src/js/index.js'])
        .pipe(terser())
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./build/js/'))
        .pipe(browserSync.stream());
}


//Поочередно запустить таски
const build = gulp.series(scripts, minhtml, styles, shakaling);

const develop = gulp.series(build, gulp.parallel(watch));
// Паралельно

//Таск вызывающий функцию styles
exports.styles = styles;
//Таск вызывающий функцию scripts
exports.img = img;
exports.images  = shakaling;
// exports.img = img;
exports.scripts = scripts;
exports.build = build;
exports.html = minhtml;
exports.watch = watch;
exports.develop = develop;